﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using Battle_city.Game;
using Battle_city.Input;
using Battle_city.Map;
using Battle_city.Models;
using Battle_city.Player;
using Xunit;
using static Battle_city.Models.MapObjectType;

namespace BattleCity_Test
{
	public class GameServiceTest
	{
		[Fact]
		public void CheckCreateTank_CorrectData()
		{
			var mapSettings = GetMapSetting();
			var respawn = mapSettings.Respawns[PlayerType.First];
			var player = new Player(SettingsFirstPlayer, PlayerType.First);
			var observable = new InputObservable();
			var gameService= new GameService(observable);

			gameService.CreateTank(mapSettings, player);

			Assert.NotNull(mapSettings.Level[respawn.X, respawn.Y].UpdateItem);
			Assert.True(mapSettings.Level[respawn.X, respawn.Y].UpdateItem is TankMapObject);
		}

		[Fact]
		public void CheckCreateTank_PlayerIsActiveTank_NotFoundTank()
		{
			var mapSettings = GetMapSetting();
			var respawn = mapSettings.Respawns[PlayerType.First];
			var player = new Player(SettingsFirstPlayer, PlayerType.First) {IsActiveTank = true};
			var observable = new InputObservable();
			var gameService= new GameService(observable);

			gameService.CreateTank(mapSettings, player);

			Assert.Null(mapSettings.Level[respawn.X, respawn.Y].UpdateItem);
			Assert.False(mapSettings.Level[respawn.X, respawn.Y].UpdateItem is TankMapObject);
		}


		[Fact]
		public void CheckCreateTank_PlayerIsLost_NotFoundTank()
		{
			var mapSettings = GetMapSetting();
			var respawn = mapSettings.Respawns[PlayerType.First];
			var player = new Player(SettingsFirstPlayer, PlayerType.First);
			player.Kill();
			player.Kill();
			var observable = new InputObservable();
			var gameService = new GameService(observable);

			gameService.CreateTank(mapSettings, player);

			Assert.Null(mapSettings.Level[respawn.X, respawn.Y].UpdateItem);
			Assert.False(mapSettings.Level[respawn.X, respawn.Y].UpdateItem is TankMapObject);
		}

		private MapSettings GetMapSetting()
		{
			var mapService = new MapService();
			var mapTypes = GetMapLevel();
			var mapSettings = mapService.GetLevel(mapTypes);
			mapSettings.Respawns = new Dictionary<PlayerType, Point>
			{
				{PlayerType.First, new Point(0, 3)}
			};
			return mapSettings;
		}

		public MapObjectType[,] GetMapLevel()
		{
			return new[,]
			{
				{Flag, Empty, Empty, Empty},
				{Empty, Empty, Empty, Empty},
				{Empty, Empty, Empty, Empty},
				{Respawn, Empty, Empty, Respawn}
			};
		}

		public static Dictionary<ConsoleKey, ActionType> SettingsFirstPlayer = new Dictionary<ConsoleKey, ActionType>
		{
			{ConsoleKey.S, ActionType.Down},
			{ConsoleKey.W, ActionType.Up},
			{ConsoleKey.A, ActionType.Left},
			{ConsoleKey.D, ActionType.Right},
			{ConsoleKey.Spacebar, ActionType.Bullet},
		};
	}
}