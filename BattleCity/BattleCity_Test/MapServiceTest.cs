using Battle_city.Map;
using Battle_city.Models;
using Xunit;
using static Battle_city.Models.MapObjectType;

namespace BattleCity_Test
{
	public class MapServiceTest
	{
		[Fact]
		public void MapLevelType_Map_CorrectLevel()
		{
			var mapService = new MapService();
			var mapTypes = GetMapLevel();
			var mapSettings = mapService.GetLevel(mapTypes);

			Assert.Equal(mapSettings.Rows, mapTypes.GetUpperBound(0)+1);
			Assert.Equal(mapSettings.Columns, mapTypes.GetUpperBound(1)+1);
		}

		public MapObjectType[,] GetMapLevel()
		{
			return new[,]
			{
				{Flag, Empty, Empty, Empty},
				{Empty, Empty, Empty, Empty},
				{Empty, Empty, Empty, Empty},
				{Respawn, Empty, Empty, Respawn}
			};
		}
	}
}
