﻿using System;
using System.Collections.Generic;
using Battle_city.Models;

namespace Battle_city.Player
{
	public interface IPlayer
	{
		public int Score { get; set; }

		public int Health { get; }
		public bool IsActiveTank { get; set; }
		public PlayerType PlayerType { get; }

		public bool IsLost();

		public void Kill();

		public Dictionary<ConsoleKey, ActionType> GetInputSettings();
	}
}
