﻿using System;
using System.Collections.Generic;
using Battle_city.Models;

namespace Battle_city.Player
{
	public class Player: IPlayer
	{
		public int Score { get; set; }
		public int Health { get; private set; } = 2;

		public bool IsActiveTank { get; set; }
		public PlayerType PlayerType { get; }

		private readonly Dictionary<ConsoleKey, ActionType> inputSettings;

		public Player(Dictionary<ConsoleKey, ActionType> inputSettings, PlayerType playerType)
		{
			this.inputSettings = inputSettings;
			PlayerType = playerType;
			IsActiveTank = false;
		}

		public bool IsLost()
		{
			return Health <= 0;
		}

		public void Kill()
		{
			Health -= 1;
			IsActiveTank = false;
		}

		public Dictionary<ConsoleKey, ActionType> GetInputSettings()
		{
			return inputSettings;
		}
	}
}
