﻿using System;

namespace Battle_city.Interfaces
{
	public interface IObjectObservable<in T>
	{
		public void Add(T item);
		public void Remove(T item);
		public void Notify(ConsoleKey keyInfo);
	}
}
