﻿namespace Battle_city.Interfaces
{
	public interface IDeathableObject
	{
		public void Hit();

		public bool IsDeath();
	}
}
