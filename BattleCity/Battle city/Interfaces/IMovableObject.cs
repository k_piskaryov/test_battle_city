﻿using System;
using Battle_city.Models;

namespace Battle_city.Interfaces
{
	public interface IMovableObject
	{
		public bool IsMove { get; set; }
		public Rotation Rotation { get; set; }

		public void Move(ConsoleKey keyInfo);
	}
}
