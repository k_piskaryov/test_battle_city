﻿using Battle_city.Models;

namespace Battle_city.Map
{
	public class MapItem
	{
		public MapObject UpdateItem { get; set; }
		public MapObject StaticItem { get; set; }

		public MapItem(MapObject updateItem, MapObject staticItem)
		{
			UpdateItem = updateItem;
			StaticItem = staticItem;
		}
	}
}
