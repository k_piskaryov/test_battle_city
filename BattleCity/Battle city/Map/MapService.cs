﻿using System.Collections.Generic;
using System.Drawing;
using Battle_city.Models;
using Battle_city.Player;
using static Battle_city.Models.MapObjectType;

namespace Battle_city.Map
{
	public class MapService
	{
		public MapSettings GetLevel(MapObjectType[,] mapTypes)
		{
			var rows = mapTypes.GetUpperBound(0) + 1;
			var columns = mapTypes.GetUpperBound(1) +1;
			var level = new MapItem[rows, columns];
			for (var i = 0; i < rows; i++)
			{
				for (var j = 0; j < columns; j++)
				{
					level[i, j] = GetMapItemByMapObjectType(mapTypes[i, j], new Point(i, j));

				}
			}

			return new MapSettings
			{
				Level = level,
				Rows = rows,
				Columns = columns,
				Respawns = new Dictionary<PlayerType, Point>
				{
					{PlayerType.First, new Point(0,9) }, // constant for this map
					{PlayerType.Second, new Point(9,9) }, // constant for this map
				}
			};
		}

		private MapItem GetMapItemByMapObjectType(MapObjectType type, Point coordinate)
		{
			return type switch
			{
				Brick => new MapItem(new BrickMapObject(coordinate), null),
				Concrete => new MapItem(new ConcreteMapObject(coordinate), null),
				Flag => new MapItem(new FlagMapObject(coordinate), null),
				River => new MapItem(null, new RiverMapObject(coordinate)),
				Respawn => new MapItem(null, new RespawnMapObject(coordinate)),
				_ => new MapItem(null, new EmptyMapObject(coordinate))
			};
		}

		public MapObjectType[,] GetMapLevel()
		{
			return new [,]
			{
				{Flag, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Flag},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, Empty, Empty, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, Empty, Empty, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Empty, Brick, Brick, Empty, Empty, River, River, Brick, Brick, Empty},
				{Respawn, Brick, Brick, Empty, Empty, Empty, Empty, Brick, Brick, Respawn},
			};
		}

	}
}
