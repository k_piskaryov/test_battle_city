﻿using System.Collections.Generic;
using System.Drawing;
using Battle_city.Player;

namespace Battle_city.Map
{
	public class MapSettings
	{
		public MapItem[,] Level { get; set; }

		public int Rows { get; set; }

		public int Columns { get; set; }

		public Dictionary<PlayerType, Point> Respawns { get; set; }
	}
}
