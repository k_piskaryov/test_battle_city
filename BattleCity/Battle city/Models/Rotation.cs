﻿namespace Battle_city.Models
{
	public enum Rotation
	{
		Up,
		Down,
		Left,
		Right
	}
}