﻿using System.Drawing;

namespace Battle_city.Models
{
	public class RiverMapObject: MapObject
	{
		public RiverMapObject(Point position) : base(position, true, false) { }
	}
}
