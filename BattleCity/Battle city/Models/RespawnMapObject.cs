﻿using System.Drawing;

namespace Battle_city.Models
{
	public class RespawnMapObject: MapObject
	{
		public RespawnMapObject(Point position) : base(position, false, false)
		{
		}
	}
}
