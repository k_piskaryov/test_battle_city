﻿using System.Drawing;

namespace Battle_city.Models
{
	public class ConcreteMapObject: MapObject
	{
		public ConcreteMapObject(Point position) : base(position, true, true) { }
	}
}
