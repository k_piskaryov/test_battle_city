﻿using System.Drawing;

namespace Battle_city.Models
{
	public class FlagMapObject: MapObject
	{
		public FlagMapObject(Point position) :
			base(position, true, true) { }
	}
}
