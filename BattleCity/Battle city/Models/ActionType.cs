﻿namespace Battle_city.Models
{
	public enum ActionType
	{
		Up,
		Down,
		Left,
		Right,
		Bullet
	}
}
