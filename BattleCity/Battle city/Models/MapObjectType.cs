﻿namespace Battle_city.Models
{
	public enum MapObjectType
	{
		Empty,
		Respawn,
		Flag,
		Brick,
		Concrete,
		River
	}
}
