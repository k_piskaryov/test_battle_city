﻿using System;
using System.Drawing;
using Battle_city.Interfaces;

namespace Battle_city.Models
{
	public class BrickMapObject: MapObject, IDeathableObject
	{
		private int health = 2;
		public BrickMapObject(Point position) :
			base(position, true, true)
		{ }

		public void Hit()
		{
			health -= 1;
			//TODO: other logic
		}

		public bool IsDeath()
		{
			return health <= 0;
		}
	}
}
