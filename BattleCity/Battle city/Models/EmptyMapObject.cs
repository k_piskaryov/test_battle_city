﻿using System.Drawing;

namespace Battle_city.Models
{
	public class EmptyMapObject: MapObject
	{
		public EmptyMapObject(Point position) : base(position, false, false)
		{

		}
	}
}
