﻿using System.Drawing;

namespace Battle_city.Models
{
	public class MapObject
	{
		public Point Position { get; set; }

		public bool IsObjectCollision { get; }

		public bool IsBulletCollision { get; }

		public MapObject(Point position, bool isObjectCollision, bool isBulletCollision)
		{
			Position = position;
			IsObjectCollision = isObjectCollision;
			IsBulletCollision = isBulletCollision;
		}
	}
}
