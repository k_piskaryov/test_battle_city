﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Battle_city.Interfaces;
using Battle_city.Player;

namespace Battle_city.Models
{
	public class TankMapObject: MapObject, IMovableObject, IDeathableObject
	{
		private int health = 1;
		public Rotation Rotation { get; set; }
		public bool IsMove { get; set; }
		public bool IsCreateBullet { get; set; }
		public IPlayer Player { get;}

		private readonly Dictionary<ActionType, Rotation> rotationMaps = new Dictionary<ActionType, Rotation>
		{
			{ActionType.Down, Rotation.Down},
			{ActionType.Up, Rotation.Up},
			{ActionType.Left, Rotation.Left},
			{ActionType.Right, Rotation.Right},
		};

		public TankMapObject(Point position, IPlayer player) : base(position, true, true)
		{
			Player = player;
			IsMove = false;
			IsCreateBullet = false;
		}

		public void Move(ConsoleKey keyInfo)
		{
			var inputSettings = Player.GetInputSettings();
			if (!inputSettings.ContainsKey(keyInfo)) return;
			var actionType = inputSettings[keyInfo];
			if (actionType == ActionType.Bullet)
			{
				IsCreateBullet = true;
			}
			else
			{
				IsMove = Rotation == rotationMaps[actionType];
				Rotation = rotationMaps[actionType];
				IsCreateBullet = false;
			}
		}

		public void Hit()
		{
			health -= 1;
			//todo: other logic
		}

		public bool IsDeath()
		{
			return health <= 0;
		}
	}
}
