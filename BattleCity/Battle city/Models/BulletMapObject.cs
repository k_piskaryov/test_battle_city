﻿using System;
using System.Drawing;
using Battle_city.Interfaces;

namespace Battle_city.Models
{
	public class BulletMapObject: MapObject, IDeathableObject
	{
		private int health = 1;

		public BulletMapObject(Point position, Rotation rotation) : base(position, true, true)
		{
			Rotation = rotation;
		}

		public Rotation Rotation { get; set; }

		public void Hit()
		{
			health -= 1;
			//TODO: other logic
		}

		public bool IsDeath()
		{
			return health <= 0;
		}
	}
}
