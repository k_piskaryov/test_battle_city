﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using Battle_city.Interfaces;
using Battle_city.Map;
using Battle_city.Models;
using Battle_city.Player;

namespace Battle_city.Game
{
	public class GameService
	{
		private const int RespawnTime = 2000;
		private const int DefaultStep = 0;
		private readonly IObjectObservable<IMovableObject> inputObservable;

		public GameService(IObjectObservable<IMovableObject> inputObservable)
		{
			this.inputObservable = inputObservable;
		}

		private readonly Dictionary<Rotation, int> moveSettings = new Dictionary<Rotation, int>
		{
			{Rotation.Down, 1},
			{Rotation.Up, -1},
			{Rotation.Left, -1},
			{Rotation.Right, 1},
		};

		public void MoveObjects(MapSettings mapSettings, CancellationToken cancellationToken)
		{
			for (var i = 0; i < mapSettings.Rows; i++)
			{
				for (var j = 0; j < mapSettings.Columns; j++)
				{
					cancellationToken.ThrowIfCancellationRequested();
					Move(mapSettings.Level[i, j].UpdateItem, mapSettings);
				}
			}
		}

		private void Move(MapObject mapObject, MapSettings mapSettings)
		{
			switch (mapObject)
			{
				case TankMapObject tank:
					CheckObjectCollision(tank, mapSettings);
					break;
				case BulletMapObject bullet:
					CheckBulletCollision(bullet, mapSettings);
					break;
			}
		}

		public void CheckObject(MapSettings mapSettings, CancellationToken cancellationToken)
		{
			for (var i = 0; i < mapSettings.Rows; i++)
			{
				for (var j = 0; j < mapSettings.Columns; j++)
				{
					cancellationToken.ThrowIfCancellationRequested();
					Check(mapSettings.Level[i, j].UpdateItem, mapSettings);
				}
			}
		}

		private void Check(MapObject mapObject, MapSettings mapSettings)
		{
			if (mapObject is TankMapObject tank)
			{
				if (tank.IsDeath())
				{
					tank.Player.Kill();
					var timer = new Timer(_ => CreateTank(mapSettings, tank.Player), null, 0, RespawnTime);
				}

				if (tank.IsCreateBullet)
				{
					var bullet = new BulletMapObject(new Point(tank.Position.X, tank.Position.Y), tank.Rotation);
					CheckBulletCollision(bullet, mapSettings);
				}
			}

			if (mapObject is IDeathableObject deathableObject)
			{
				if (deathableObject.IsDeath())
				{
					mapObject = null;
				}
			}
		}

		public void CreateTank(MapSettings mapSettings, IPlayer player)
		{
			if (!player.IsActiveTank && !player.IsLost())
			{
				var position = mapSettings.Respawns[player.PlayerType];
				var tank = new TankMapObject(position, player);
				inputObservable.Add(tank);
				mapSettings.Level[position.X, position.Y].UpdateItem = tank;
				player.IsActiveTank = true;
			}
		}

		private void CheckObjectCollision(TankMapObject tank, MapSettings mapSettings)
		{
			if (tank.IsMove)
			{
				var step = moveSettings[tank.Rotation];
				var nextPositionX = tank.Position.X + (tank.Rotation == Rotation.Down || tank.Rotation == Rotation.Up
					? step
					: DefaultStep);
				var nextPositionY = tank.Position.Y + (tank.Rotation == Rotation.Left || tank.Rotation == Rotation.Right
					? step
					: DefaultStep);

				if ((nextPositionX >= 0 && nextPositionX < mapSettings.Rows) &&
					(nextPositionY >= 0 && nextPositionY < mapSettings.Columns))
				{
					var mapItem = mapSettings.Level[nextPositionX, nextPositionY];
					if ((mapItem.UpdateItem == null ||
							mapItem.UpdateItem != null && !mapItem.UpdateItem.IsObjectCollision) &&
						(mapItem.StaticItem == null ||
							mapItem.StaticItem != null && !mapItem.StaticItem.IsObjectCollision))
					{
						mapSettings.Level[tank.Position.X, tank.Position.Y].UpdateItem = null;
						tank.Position = new Point(nextPositionX, nextPositionY);
						mapItem.UpdateItem = tank;
						tank.IsMove = false;
					}
				}
			}
		}

		private void CheckBulletCollision(BulletMapObject bullet, MapSettings mapSettings)
		{
			var step = moveSettings[bullet.Rotation];
			var nextPositionX =
				bullet.Position.X + (bullet.Rotation == Rotation.Down || bullet.Rotation == Rotation.Up
					? step
					: DefaultStep);
			var nextPositionY = bullet.Position.Y +
				(bullet.Rotation == Rotation.Left || bullet.Rotation == Rotation.Right ? step : DefaultStep);

			if ((nextPositionX >= 0 && nextPositionX < mapSettings.Rows) &&
				(nextPositionY >= 0 && nextPositionY < mapSettings.Columns))
			{
				var mapItem = mapSettings.Level[nextPositionX, nextPositionY];
				if ((mapItem.UpdateItem == null ||
						mapItem.UpdateItem != null && !mapItem.UpdateItem.IsBulletCollision) &&
					(mapItem.StaticItem == null || mapItem.StaticItem != null && !mapItem.StaticItem.IsBulletCollision))
				{
					mapSettings.Level[bullet.Position.X, bullet.Position.Y].UpdateItem = null;
					bullet.Position = new Point(nextPositionX, nextPositionY);
					mapItem.UpdateItem = bullet;
				}
				else
				{
					if (mapItem.UpdateItem is IDeathableObject deathUpdateItem)
					{
						deathUpdateItem.Hit();
					}

					if (mapItem.StaticItem is IDeathableObject deathStaticItem)
					{
						deathStaticItem.Hit();
					}

					bullet.Hit();
				}
			}
		}
	}
}