﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Battle_city.Input;
using Battle_city.Map;
using Battle_city.Player;

namespace Battle_city.Game
{
	public class GameSettingsService
	{
		private MapSettings mapSettings;
		private List<IPlayer> players;
		private readonly InputService inputService;
		private readonly GameService gameService;
		private CancellationTokenSource cancellationTokenSource;

		public GameSettingsService(
			InputService inputService,
			GameService gameService)
		{
			this.inputService = inputService;
			this.gameService = gameService;
		}

		public void SetupGame(
			MapSettings mapSettings,
			List<IPlayer> players)
		{
			cancellationTokenSource = new CancellationTokenSource();
			this.mapSettings = mapSettings;
			this.players = players;
			foreach (var player in players)
			{
				gameService.CreateTank(mapSettings, player);
			}
		}

		public async Task RunGame()
		{
			var cancellationToken = cancellationTokenSource.Token;
			await Task.WhenAll(Task.Run(()=> Game(cancellationToken), cancellationToken),
				Task.Run(() => inputService.ReadKey(cancellationToken), cancellationToken));
		}

		private void Game(CancellationToken cancellationToken)
		{
			while (true)
			{
				if (cancellationToken.IsCancellationRequested)
				{
					break;
				}
				gameService.MoveObjects(mapSettings, cancellationToken);
				gameService.CheckObject(mapSettings, cancellationToken);
				this.CheckIsWin();
				//TODO: paint UI
				Thread.Sleep(100);
			}
		}

		private void CheckIsWin()
		{
			foreach (var player in players)
			{
				if (player.IsLost())
				{
					Console.WriteLine($"Win is player - {player.PlayerType}");
					cancellationTokenSource.Cancel();
				}
			}
		}
	}
}
