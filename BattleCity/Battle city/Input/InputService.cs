﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Battle_city.Interfaces;

namespace Battle_city.Input
{
	public class InputService
	{
		private readonly IObjectObservable<IMovableObject> inputObservable;

		public InputService(IObjectObservable<IMovableObject> inputObservable)
		{
			this.inputObservable = inputObservable;
		}

		public void ReadKey(CancellationToken cancellationToken)
		{
			while (true)
			{
				if (cancellationToken.IsCancellationRequested)
				{
					break;
				}

				if (!Console.KeyAvailable)
				{
					var key = Console.ReadKey(true);
					inputObservable.Notify(key.Key);
				}
			}
		}
	}
}