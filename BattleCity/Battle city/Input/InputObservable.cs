﻿using System;
using System.Collections.Generic;
using Battle_city.Interfaces;

namespace Battle_city.Input
{
	public class InputObservable: IObjectObservable<IMovableObject>
	{
		private readonly List<IMovableObject> observers;

		public InputObservable()
		{
			observers = new List<IMovableObject>();
		}

		/// <inheritdoc />
		public void Add(IMovableObject item)
		{
			observers.Add(item);
		}

		public void Remove(IMovableObject item)
		{
			observers.Remove(item);
		}

		public void Notify(ConsoleKey keyInfo)
		{
			foreach (var observer in observers)
			{
				observer.Move(keyInfo);
			}
		}
	}
}
