﻿using Battle_city.Game;
using Battle_city.Input;
using Battle_city.Interfaces;
using Battle_city.Map;
using Microsoft.Extensions.DependencyInjection;

namespace Battle_city
{
	public static class DI
	{
		public static ServiceProvider ServiceProvider;

		public static void RegisterServices()
		{
			var services = new ServiceCollection();
			services.AddSingleton<IObjectObservable<IMovableObject>, InputObservable>();
			services.AddSingleton<GameSettingsService>();
			services.AddSingleton<GameService>();
			services.AddSingleton<MapService>();
			services.AddSingleton<InputService>();
			ServiceProvider = services.BuildServiceProvider(true);
		}
	}
}
