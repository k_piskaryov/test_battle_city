﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Battle_city.Game;
using Battle_city.Input;
using Battle_city.Map;
using Battle_city.Models;
using Battle_city.Player;
using Microsoft.Extensions.DependencyInjection;

namespace Battle_city
{
	class Program
	{
		static async Task Main(string[] args)
		{
			DI.RegisterServices();
			var gameSettingsService = DI.ServiceProvider.GetService<GameSettingsService>();
			var mapService = DI.ServiceProvider.GetService<MapService>();
			var players = new List<IPlayer>()
			{
				new Player.Player(SettingsFirstPlayer, PlayerType.First),
				new Player.Player(SettingsSecondPlayer, PlayerType.Second),
			};
			var mapSettings = mapService.GetLevel(mapService.GetMapLevel());

			gameSettingsService.SetupGame(mapSettings, players);

			await gameSettingsService.RunGame();

		}


		public static Dictionary<ConsoleKey, ActionType> SettingsFirstPlayer = new Dictionary<ConsoleKey, ActionType>
		{
			{ConsoleKey.S, ActionType.Down},
			{ConsoleKey.W, ActionType.Up},
			{ConsoleKey.A, ActionType.Left},
			{ConsoleKey.D, ActionType.Right},
			{ConsoleKey.Spacebar, ActionType.Bullet},
		};

		public static Dictionary<ConsoleKey, ActionType> SettingsSecondPlayer = new Dictionary<ConsoleKey, ActionType>
		{
			{ConsoleKey.DownArrow, ActionType.Down},
			{ConsoleKey.UpArrow, ActionType.Up},
			{ConsoleKey.LeftArrow, ActionType.Left},
			{ConsoleKey.RightArrow, ActionType.Right},
			{ConsoleKey.Enter, ActionType.Bullet},
		};
	}
}